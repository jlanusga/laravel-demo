<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Validator;
use Auth;

use App\Arstist;
use App\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    public function checklogin(Request $request){
        $this->validate($request, [
            'email'     =>      'required|email',
            'password'  =>      'required|AlphaNum|min:3'
        ]);

        $user_data = array(
            'email'     => $request->get('email'),
            'password'  => $request->get('password')
        );


        $name = $request->session()->get('name', 'default');
        $email = $request->session()->get('email', 'default');
        $password = $request->session()->get('password', 'default');

        $whereData = [//where array syntax
            ['email', $email],
            ['password', $password]
        ];
        //query
        $data = DB::table('artists')->get(); 

        if(Auth::attempt($user_data)){
            return view('profile', ['art' => $data]);
        }
        else{
            return back()->with('error', 'Wrong email or password');
        }
    }

    public function logout(){
        Auth::logout();
        return view('login')->with('flashdata', 'You have logged out');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
