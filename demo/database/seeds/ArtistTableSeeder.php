<?php

use Illuminate\Database\Seeder;
use App\Artist;

class ArtistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $desc_1 = 'Mars Ravelo was a Filipino comic book cartoonist and graphic novelist.';

        $desc_2 = 'Rico Rene Granados Blanco is a Filipino singer, songwriter, multi-instrumentalist, record producer, actor, endorser and entrepreneur.';

        $desc_3 = 'Eleandre "Ely" Basiño Buendia is a Filipino musician, writer and director who gained fame as guitarist, songwriter and lead vocalist of the popular Filipino rock band Eraserheads.';
        
        $desc_4 = 'Beethoven Michael del Valle Bunagan (born December 17, 1969), known by his stage name Michael V. and also known as "Bitoy".';

        $desc_5 = 'Gerry is known for his first self-written comic Wasted.';

        Artist::create([
            'artist_name'  =>  'Mars Ravelo',
            'artist_desc' =>  $desc_1,
            'artist_dp' =>  'dp_1.png'
        ]);

        Artist::create([
            'artist_name'  =>  'Rico Blanco',
            'artist_desc' =>  $desc_2,
            'artist_dp' =>  'dp_2.png'
        ]);

        Artist::create([
            'artist_name'  =>  'Ely Buendia',
            'artist_desc' =>  $desc_3,
            'artist_dp' =>  'dp_3.png'
        ]);

        Artist::create([
            'artist_name'  =>  'Michael V.',
            'artist_desc' =>  $desc_4,
            'artist_dp' =>  'dp_4.png'
        ]);

        Artist::create([
            'artist_name'  =>  'Gerry Alanguilan',
            'artist_desc' =>  $desc_5,
            'artist_dp' =>  'dp_5.png'
        ]);
    }
}
