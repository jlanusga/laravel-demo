<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Artists;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$name_1 = 'Perfecto De Castro';
    	$name_2 = 'Rico Blanco';
    	$name_3 = 'Francisco Gaudencio Lope Belardo Mañalac';
    	$name_4 = 'Nathan Azarcon';
    	$name_5 = 'Mark Escueta';

        User::create([
            'name'  =>  $name_1,
            'email' =>  'perfdecastro@gmail.com',
            'password'  =>  Hash::make('password'),
            'remember_token'    =>  str_random(10)
        ]);

        User::create([
            'name'  =>  $name_2,
            'email' =>  'rblanco@gmail.com',
            'password'  =>  Hash::make('password'),
            'remember_token'    =>  str_random(10)
        ]);

        User::create([
            'name'  =>  $name_3,
            'email' =>  'bamboo@gmail.com',
            'password'  =>  Hash::make('password'),
            'remember_token'    =>  str_random(10)
        ]);

        User::create([
            'name'  =>  $name_4,
            'email' =>  'nazarcon@gmail.com',
            'password'  =>  Hash::make('password'),
            'remember_token'    =>  str_random(10)
        ]);

        User::create([
            'name'  =>  $name_5,
            'email' =>  'mescueta@gmail.com',
            'password'  =>  Hash::make('password'),
            'remember_token'    =>  str_random(10)
        ]);
    }
}
