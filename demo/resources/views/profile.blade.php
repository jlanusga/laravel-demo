<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.css">
    <br><br>
    <title>Welcome Artist-san!</title>
  </head>
  <style type="text/css">
    .btn-xs {
        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
    }
    #ellipse{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        padding-bottom: 5px;
    }
    .auto {
      display: block;
      margin: 3px;
      height: 32px;
      overflow: hidden;
      transition: all .3s ease;
    }
    #prod-image{height: 15rem;}
    .txt{color: #FF7518;}
    tr{background-color: #f8f9f9;}
    tr:hover{background-color: #e4e4e4;}
  </style>
  <body>

    <a class="btn btn-warning btn-block" href="{{ url('/main/logout') }}"><i class="fa fa-shopping-basket"></i>&nbsp;&nbsp;Log Out</a>

    <!----=====CONTENT====----->


        <div class="container" style="padding: 5px;">
            <div class="row">
            @foreach($art as $row)
                <div class="col-md-3 card-hover" style="padding: 5px;">
                    <div class="card-columns-fluid">
                        <div class="card bg-light">
                            <img class="card-img-top" src="/storage/{{ $row->artist_dp }}" alt="Card image cap" id="prod-image">

                            <div class="card-body">
                                <h3 class="card-title auto">{{ $row->artist_name }}</h3>
                                <p class="card-text" id="ellipse"><b>Description: </b>{{ $row->artist_desc }}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div><br>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>      
  </body>
</html>