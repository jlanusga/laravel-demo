<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.css">
    <br><br>
    <title>Sign In</title>
  </head>

  <style type="text/css">
  .strike {
      display: block;
      text-align: center;
      overflow: hidden;
      white-space: nowrap; 
  }

  .strike > span {
      position: relative;
      display: inline-block;
  }

  .strike > span:before,
  .strike > span:after {
      content: "";
      position: absolute;
      top: 50%;
      width: 9999px;
      height: 5px;
      background: #343d46;
  }

  .strike > span:before {
      right: 100%;
      margin-right: 15px;
  }

  .strike > span:after {
      left: 100%;
      margin-left: 15px;
  }
  .form-control{background-color: #e5e5e5}

  </style>

  <body>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-centered">

    <h1 class="header1">Sign In</h1>

    <p>Fill the form to Login!</p>

    @if(isset(Auth::user()->email))
      <script>window.location="/main/successlogin"</script>
    @endif

    @if($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif

    <!-- validator -->
    @if (count($errors) > 0)
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form action="{{ url('/main/userprofile') }}" method="POST">
      {{ csrf_field() }} <!--for handle multiple exception-->
      <label for="username" class="label-default">Email:</label>
      <div class="form-group">
          <i class="fa fa-user icon"></i>
          <input class="form-control" name="email" id="email" type="email" placeholder="Enter Email">
      </div>

      <label for="password" class="label-default">Password:</label>
      <div class="form-group">
          <i class="fa fa-lock icon"></i>
          <input class="form-control" name="password" id="password" type="password" placeholder="Enter Password">
      </div>

    <div class="custom-control custom-checkbox">
      <input type="checkbox" class="custom-control-input" name="customCheck1" id="customCheck1">
      <label class="custom-control-label" for="customCheck1">&nbsp;Remember me</label>
    </div>

      <br>
      <div>
          <button class="btn btn-primary" name="login-btn" style="border-radius: 0.6rem;">Sign In</button>
          <button class="btn btn-danger"  name="cancel-btn" type="reset" style="border-radius: 0.6rem;">Cancel</button>
      </div>
      </form>
          <br>
          
              <div class="or">
                
                <div class="strike">
                   <span style="color:#343d46; font-weight: bold; font-size:1.3rem; font-family: 'Permanent Marker', cursive;">OR</span>
                </div>

                <br>
                <a class="nav-link" href="#" style="text-align: center;"><button class="btn btn-secondary" name="signup-btn" style="border-radius: 0.6rem;">Create an account</button></a>
                <br>

                <div style="text-align: center;">
                  <a href="#" class="secondary-btn">Forgot Password?</a>
                </div>  
              </div>

            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  </body>
</html>